#!/bin/bash
#

cfitsiolib=cfitsio3410
wcslib=wcstools-3.9.5
gsllib=gsl-2.4

ltversion=lenstool-7.0




ldir=`pwd`

# Install wcstools
if [ -d "$wcslib" ]; then
    echo "Folder $wcslib exists! Skipping compilation."
else
    echo "Folder $wcslib does not exist! Dowloading and compiling."
    wget -O - ftp://cfa-ftp.harvard.edu/pub/gsc/WCSTools/$wcslib.tar.gz | tar zx
    cd $wcslib
    make all
    cd ..
fi



# Install Cfitsio
if [ -d "$cfitsiolib" ]; then
    echo "Folder $cfitsiolib exists! Skipping compilation"
else
    echo "Folder $cfitsiolib does not exist! Dowloading and compiling."
    wget -O - http://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/$cfitsiolib.tar.gz | tar zx
    cd cfitsio
    ./configure --prefix=$ldir''/$cfitsiolib/
    make
    make install
    cd ..
    rm -r cfitsio
fi


  
    
## Install GSL
if [ -d "$gsllib" ]; then
    echo "Folder $gsllib exists! Skipping compilation"
else
    echo "Folder $gsllib does not exist! Dowloading and compiling."
    wget -O - ftp://ftp.gnu.org/gnu/gsl/$gsllib''.tar.gz | tar zx
    mv $gsllib gsl-src
    cd gsl-src
    ./configure --prefix=$ldir''/$gsllib''/
    make
    make install
    export LD_LIBRARY_PATH=`pwd`/$gsllib''/lib 
    cd ..
    rm -rf gsl-src
fi



## Get Lenstool source code and install it
if [ -d "$ltversion" ]; then
    echo "Folder $ltversion exists! Skipping download."
else
    echo "Folder $ltversion does not exist! Dowloading and compiling."
    wget -O - https://projets.lam.fr/attachments/download/4243/$ltversion''.tar.gz | tar zx
fi
cd $ltversion
./configure \
    --with-wcslib-lib-path=$ldir''/$wcslib/libwcs \
    --with-wcslib-include-path=$ldir''/$wcslib/libwcs \
    --with-cfitsio-include-path=$ldir''/$cfitsiolib/include \
    --with-cfitsio-lib-path=$ldir''/$cfitsiolib/lib \
    --with-gsl-include-path=$ldir''/$gsllib/include \
    --with-gsl-lib-path=$ldir''/$gsllib/lib
sed -i 's/NGGMAX \t128/NGGMAX \t256/g' include/dimension.h
make


echo >> $HOME/.bashrc
echo >>	$HOME/.bashrc
echo "# Lenstool" >> $HOME/.bashrc
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:'$ldir/$gsllib'/lib' >> $HOME/.bashrc
echo export LENSTOOL_DIR=$ldir/$ltversion >> $HOME/.bashrc
echo 'export PATH=$PATH:$LENSTOOL_DIR/src:$LENSTOOL_DIR/perl:$LENSTOOL_DIR/utils' >> $HOME/.bashrc




